package userInterface;

import calendar.Event;
import calendar.CalendarLogic;

import java.util.Date;
import java.util.Calendar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;

/**
 * Class for displaying events in the graphical user interface.
 * @author Hurakano, Janfifian
 *
 */
class EventDisplay extends JFrame
{
	private Event event;
	private CalendarLogic logic;
	private IUserInterface ui;
	
	private JTextArea textArea;
	private JTextField textField;
	private JSpinner hourSpinner;
	private JSpinner minuteSpinner;
	
/**
 * Launches the EventDisplay window for a selected event, allowing to manipulate
 * its properties, save it in the container or remove it entirely.
 * @param ev Event to be modified.
 * @param logi Underlaying Business Logic Layer for the application.
 */
	public EventDisplay(Event ev, CalendarLogic logi, IUserInterface userInt)
	{
		event = ev;
		logic = logi;
		ui = userInt;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(300, 450);
		setLayout(null);
		
		textArea = new JTextArea(ev.getDescription());
		textArea.setBounds(10, 40, 280, 100);
		JLabel txtarLabel = new JLabel("Opis:");
		txtarLabel.setBounds(10, 10, 50, 15);
		add(txtarLabel);
		add(textArea);
		
		textField = new JTextField(ev.getPlace());
		textField.setBounds(10, 190, 280, 30);
		JLabel txtfiLabel = new JLabel("Miejsce:");
		txtfiLabel.setBounds(10, 170, 80, 15);
		add(txtfiLabel);
		add(textField);
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		JLabel dateLab = new JLabel("Data: " + formatter.format(ev.getDate()));
		dateLab.setBounds(10, 240, 280, 15);
		add(dateLab);
		
		
		int hours = 0, minutes = 0;
		if(ev.getRemind() != null)
		{
			long evTime = ev.getDate().getTime();
			long remind = ev.getRemind().getTime();
		
			hours = (int)(evTime-remind)/3600000;
			minutes = (int)(evTime-remind)/60000%60;
		}
		
		JLabel remindLab = new JLabel("Przypomnij przed");
		remindLab.setBounds(10, 280, 200, 15);
		JLabel hourLab = new JLabel("h");
		JLabel minutLab = new JLabel("m");
		hourLab.setBounds(62, 300, 15, 25);
		minutLab.setBounds(127, 300, 15, 25);
		add(remindLab);
		add(hourLab);
		add(minutLab);
		
		hourSpinner = new JSpinner(new SpinnerNumberModel(hours, 0, 99, 1));
		hourSpinner.setBounds(10, 300, 50, 25);
		minuteSpinner = new JSpinner(new SpinnerNumberModel(minutes, 0, 99, 1));
		minuteSpinner.setBounds(75, 300, 50, 25);
		
		add(hourSpinner);
		add(minuteSpinner);
		
		JButton buttSave = new JButton("Zapisz");
		buttSave.setBounds(10, 350, 80, 25);
		buttSave.addActionListener(new ActionListener()
		{
				public void actionPerformed(ActionEvent e)
				{
					saveEvent();
				}
		});
		add(buttSave);
		
		JButton buttOk = new JButton("OK");
		buttOk.setBounds(95, 350, 80, 25);
		buttOk.addActionListener(new ActionListener()
		{
				public void actionPerformed(ActionEvent e)
				{
					closeWindow();
				}
		});
		add(buttOk);
		
		JButton buttDel = new JButton("Usuń");
		buttDel.setBounds(180, 350, 80, 25);
		buttDel.addActionListener(new ActionListener()
		{
				public void actionPerformed(ActionEvent e)
				{
					deleteEvent();
				}
		});
		add(buttDel);
		
		setVisible(true);
	}
/**
 * Saves the event in the underlaying container.
 */
	private void saveEvent()
	{
		String desc = textArea.getText();
		String plc = textField.getText();
		int hour = (int)hourSpinner.getValue();
		int min = (int)minuteSpinner.getValue();
		
		event.setDescription(desc);
		event.setPlace(plc);
		
		if(hour != 0 || min != 0)
			event.setRemind(hour, min);
		else
			event.deleteRemind();
			
		ui.updateEvent();
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

/**
 * Closes the window.
 */
	private void closeWindow()
	{
		ui.updateEvent();
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Removes the event.
	 */
	private void deleteEvent()
	{
		logic.removeEvent(event);
		ui.updateEvent();
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
}
