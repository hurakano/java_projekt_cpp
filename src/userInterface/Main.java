package userInterface;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import calendar.*;
/**
 * @author JanFifian
 * 
 * Class, which launches the whole project.
 */
class Main
{
	/**
	 * Launch the project
	 * @param args four String arguments are taken into consideration.
	 * 		First one is a switch "-b", which allows to launch the project
	 * 		with instant connection to the database.
	 * 
	 * 		Second, third and fourth ones are database path, username and password
	 * 		for the database respectively.
	 */
	public static void main(String args[])
	{
		boolean database = false;
		int interfaceType;
		IUserInterface ui;
		
		CalendarLogic cal = new CalendarLogic();
		
		if(args.length > 0 && args[0].equals("-t"))
			interfaceType = 1;
		else
			interfaceType = 2;
		
		
		if(args.length > 1)
		{
			if(args[1].equals("-b"))
			{
				database = true;
				cal.setDatabaseAccess(args[2], args[3], args[4]);;
			}
		}
		
		if(interfaceType == 1)
			ui = new TextInterface(cal);
		else 
			ui = new GraphicalInterface(cal, database);
		
		ui.useMainScreen();
	}
}
