package userInterface;

import calendar.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.List;
/**
 * @author JanFifian
 * 
 * Class, which implements Text User Interface. 
 * Allows user to modify the current state of 
 * the application and preview the content within.
 */
public final class TextInterface implements IUserInterface, Remindable{
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private Scanner reader = new Scanner(System.in);
	
	private CalendarLogic logic;
	/**
	 * Constructs a text interface as a top, presentation layer for a
	 * given two underlaying data and business logic layers.
	 * 
	 * @param calLog logic layer, for which text interface is provided. 
	 */
	public TextInterface(CalendarLogic calLog)
	{
		logic = calLog;
		logic.setRemindable(this);
	}
	/**
	 * This method provides the basic functionalities of our organiser.
	 * It shows the main control interface, which allows user to use other functions.
	 */
	@Override
	public void useMainScreen() {
		boolean showTitle = true;
		if (showTitle) {
			System.out.println("  ______                                          __                               ");
			System.out.println(" /      \\                                        |  \\                              ");
			System.out
					.println("|  $$$$$$\\  ______    ______    ______   _______   \\$$ ________   ______    ______  ");
			System.out
					.println("| $$  | $$ /      \\  /      \\  |      \\ |       \\ |  \\|        \\ /      \\  /    ");
			System.out.println(
					"| $$  | $$|  $$$$$$\\|  $$$$$$\\  \\$$$$$$\\| $$$$$$$\\| $$ \\$$$$$$$$|  $$$$$$\\|  $$$$$$\\  ");
			System.out.println(
					"| $$  | $$| $$   \\$$| $$  | $$ /      $$| $$  | $$| $$  /    $$ | $$    $$| $$   \\$$  ");
			System.out
					.println("| $$__/ $$| $$      | $$__| $$|  $$$$$$$| $$  | $$| $$ /  $$$$_ | $$$$$$$$| $$        ");
			System.out.println(
					" \\$$    $$| $$       \\$$    $$ \\$$    $$| $$  | $$| $$|  $$    \\ \\$$     \\| $$        ");
			System.out.println(
					"  \\$$$$$$  \\$$       _\\$$$$$$$  \\$$$$$$$ \\$$   \\$$ \\$$ \\$$$$$$$$  \\$$$$$$$ \\$$        ");
			System.out
					.println("                    |  \\__| $$                                                        ");
			System.out
					.println("                     \\$$    $$                                                        ");
			System.out
					.println("                       \\$$$$$$                                                        ");
		}
		showAvailableOptions();

		showTitle = false;
		try {
			char choice = reader.nextLine().charAt(0);
			while (choice != 'Q') {
				choice = performAction(choice);
			}
		} catch (Exception e) {
			System.out.println("Nieprawidłowy wybór, wyjście z programu...");
		}

	}
	/**
	 * Shows available options for the user to choose from. This method actually
	 * does not let user choose anything, it only prints out the information 
	 * about options to choose from. 
	 */
	public void showAvailableOptions() {
		System.out.println("Wybierz jedną z dostępnych opcji: ");
		System.out.println("[0]: Wyświetla informacje o programie;");
		System.out.println("[1]: Wyświetla wszystkie zapisane zdarzenia;");
		System.out.println("[2]: Wyświetla zdarzenia z wybranego interwału czasowego;");
		System.out.println("[3]: Dodaje nowe zdarzenie do organizera;");
		System.out.println("[4]: Modyfikuje wybrane zapisane wcześniej zdarzenie;");
		System.out.println("[5]: Usuwa wybrane zdarzenie z organizera;");
		System.out.println("[6]: Usuwa z organizera przestarzałe i nieaktualne zdarzenia;");
		System.out.println("[7]: Wyświetla posortowane zdarzenia;");
		System.out.println("[8]: Zapisuje do pliku XML stan organizera;");
		System.out.println("[9]: Odczytuje z pliku XML stan organizera;");
		System.out.println("[Q]: Kończy działanie programu;");
	}
	/**
	 * Saves the current organiser state to XML file with proper filename.
	 * 
	 * @param filename 	name of the file, under which program will attempt to save the current state.
	 */
	@Override
	public void saveXML(String filename) {
		
		if(logic.saveToXML(filename) == 0)
			System.out.println("Zapisano pomyślnie");
		else
			System.out.println("Wystąpił‚ błąd zapisu");
	}
	/**
	 * Loads the organiser state from XML file with proper filename.
	 * 
	 * @param filename 	name of the file, under which program will attempt to save the current state.
	 */
	@Override
	public void loadXML(String filename) {
		
		if(logic.loadFromXML(filename) == 0)
			System.out.println("Wczytano pomyślnie");
		else
			System.out.println("Wystąpił błąd odczytu");
	}
	
		/**
	 * Saves the current organiser state to ods file with proper filename.
	 * 
	 * @param filename 	name of the file, under which program will attempt to save the current state.
	 */
	@Override
	public void saveOds(String filename) {
		
		if(logic.saveToOds(filename) == 0)
			System.out.println("Zapisano pomyślnie");
		else
			System.out.println("Wystąpił‚ błąd zapisu");
	}
	
	
	/**
	 *  Sorts all the events with respect to date.
	 * 
	 */
	public void sortEvents() {
		logic.sort();

	}
	
	/** 
	 * Shows all events from a given time interval.
	 * 
	 * @param fromWhen		Starting point of time interval
	 * @param untilWhen		Ending point of time interval
	 */
	public void showEvents(Date fromWhen, Date untilWhen) {
		
		List<Event> events = logic.getEvents(fromWhen, untilWhen);
		
		for(Event ev : events)
			System.out.println(ev.toString());

	}

	/**
	 * Shows all events currently held in the organiser.
	 * If no sorting method was applied beforehand, the
	 * resulting order is determined by the order of
	 * adding the events to the database.
	 */
	public void showAllEvents() {
		
		int order = 0;
		List<Event> events = logic.getAllEvents();
		
		for(Event ev : events)
		{
			System.out.println(order + ": " + ev.toString());
			order++;
		}
	}
	/**
	 * Shows "About Organiser" Window
	 */
	@Override
	public void showAboutWindow() {
		System.out.println(
				"********************************************\n"+
				"* Organizer - prosty sposób na zarządzanie *\n"+
				"*       Twoim czasem!                      *\n"+
				"*                                          *\n"+
				"*      Autorzy: Hubert Sosnowski (Hurakano)*\n"+
				"*               Filip Turoboś (Janfifian)  *\n"+
				"*                                          *\n"+
				"*                                          *\n"+
				"********************************************\n"
				);
	}
	/**
	 * Starts the Creator of Events in order to add new event
	 */
	@Override
	public void addNewEvent() {
		
		String desc, place;
		Date date;
		
		try
		{
			System.out.println("Podaj opis: ");
			desc = reader.nextLine();
			System.out.println("Podaj miejsce: ");
			place = reader.nextLine();
			System.out.println("Podaj datę (DD/MM/YYYY HH:MM): ");
			date = formatter.parse(reader.nextLine());
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return;
		}
		
		logic.addEvent(desc, place, date);
	}
	/**
	 * Removes all Events which are older than given date.
	 * @param olderThan date, for which events preceeding it will be removed.
	 */
	@Override
	public void removeEvents(Date olderThan) {
		
		logic.removeOlder(olderThan);

	}

/**
 * Starts the Event Removal Creator.
 */
	public void removeSpecificEvent() {
/*
		System.out.println("Wybierz numer zdarzenia do usunięcia:");
		this.showAllEvents();
*/
	}
	/**
	 * Starts Event Update Creator.
	 */
	public void updateEvent() {
		// TODO Auto-generated method stub

	}
	/**
	 * This method allows user to run the selected method 
	 * from IUserInterface implemented in this class.
	 * 
	 * @param choice - number of option to be chosen. Correct
	 * entries for this field consist of a single number from
	 * 0 to 9 or a single letter Q.
	 * 
	 * @return - in case of standard input, after executing 
	 * the desired method the program proceeds to take another
	 * value from user and returns it as a result. In the situation,
	 * where user input is not consistent with the desired one,
	 * another value is taken or, in case where input fails to be a
	 * character, the Exception is risen and caught, closing the program.
	 */
	private char performAction(char choice) {
		Date start;
		Date end;
		switch (choice) {
		case '0':
			this.showAboutWindow();
			break;
		case '1':
			this.showAllEvents();
			break;
		case '2':
			try {
				System.out.println("Podaj początek przedziału czasowego (DD/MM/YYYY):");
				start = formatter.parse(reader.nextLine());
				System.out.println("Podaj koniec przedziału czasowego (DD/MM/YYYY):");
				end = formatter.parse(reader.nextLine());
				this.showEvents(start, end);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			break;
		case '3':
			this.addNewEvent();
			break;
		case '4':
			this.updateEvent();
			break;
		case '5':
			this.removeSpecificEvent();
			break;
		case '6':
			try {
				System.out.println("Podaj datę końcową przedziału usuwania przestarzałych zdarzeń (DD/MM/YYYY):");
				end = formatter.parse(reader.nextLine());
				this.removeEvents(end);
				} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			break;
		case '7':
			this.sortEvents();
			break;
		case '8':
			System.out.println("Podaj nazwę pliku docelowego:");
			this.saveXML(reader.nextLine());
			break;
		case '9':
			System.out.println("Podaj nazwę pliku ze stanem kalendarza:");
			this.loadXML(reader.nextLine());
			break;
		default:
			return choice;
		}
		char newChoice = 'Q';
		try {
			newChoice = reader.nextLine().charAt(0);
		} catch (Exception e) {
			System.out.println("Nieprawidłowy wybór, wyjście z programu...");
			return 'Q';
		}

		return newChoice;
	}
/**
 * Reminds the user about upcoming event.
 */
	@Override
	public void remind(Event ev)
	{
		System.out.println("Pamiętaj o: "+ev.getDescription());
	}
}
