package userInterface;

import java.util.Date;
/**
 * An interface which is used to coalesce both graphical and console UI.
 * @author JanFifian
 */
public interface IUserInterface {
/**
 * This method implements basic functionalities of our organiser.
 * It shows the main control interface, which allows user to use other functions.
 */
	public void useMainScreen();
/**
 * Saves the current organiser state to XML file with proper filename.
 * 
 * @param filename 	name of the file, under which program will attempt to save the current state.
 */
	
	public void saveXML(String filename);

/**
 * Loads the organiser state from XML file with proper filename.
 * 
 * @param filename 	name of the file, under which program will attempt to save the current state.
 */
	
	public void loadXML(String filename);

/**
 * Saves the current organiser state to ods file with proper filename.
 * 
 * @param filename 	name of the file, under which program will attempt to save the current state.
 */
 	public void saveOds(String filename);
	
	
/**
 * Shows "About Organiser" Window
 * 
 */
	
	public void showAboutWindow();

/**
 * Starts the Creator of Events in order to add new event
 */
	
	public void addNewEvent();

/**
 * Removes all Events which are older than given date.
 * @param olderThan date, for which events preceeding it will be removed.
 */
	
	public void removeEvents(Date olderThan);

/**
 * Starts Event Update Creator.
 */
	
	public void updateEvent();
}
