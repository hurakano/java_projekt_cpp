package userInterface;

import calendar.*;

import java.awt.EventQueue;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.JSpinner;
import java.awt.Toolkit;

import java.awt.GridLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerListModel;
import javax.swing.event.*;

import java.util.Calendar;
import java.util.ArrayList;
import javax.swing.JLabel;


/**
 * Class which provides access to GUI along with the connection to Business Logic Layer.
 * It allows user to make use of the application.
 * @author Janfifian, Hurakano
 *
 */
public class GraphicalInterface implements IUserInterface, Remindable {

	private JFrame frame;
	private JButton buttons[] = new JButton[42];
	private List<JButton> buttons2 = new ArrayList<JButton>();
	private Date currentlySelectedDate;
	private	CalendarLogic logic;
	
	private boolean databaseState = false;
	
	private Color buttonColor = null;
	private Color calendarColor = null;
	private Color activeCalendarColor = new Color(150, 0, 240, 200);
	
	
	/**
	 * Launches the application GUI.
	 */
	public void useMainScreen() {

		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application interface.
	 */
	public GraphicalInterface(CalendarLogic cal, boolean ifDatabase) {
		logic = cal;
		logic.setRemindable(this);
		databaseState = ifDatabase;
		
		if(databaseState)
			logic.loadFromDatabase();
	}

//////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		if(databaseState)
		{
			frame.addWindowListener(new WindowAdapter()
			{
    			public void windowClosing(WindowEvent e)
    			{
					logic.saveToDatabase();
    			}
			});
		}
	///////////////////////////////////////
	//keyboard events
	JPanel panel = new JPanel();

	panel.getActionMap().put("addEvent", new AbstractAction()
	{
		public void actionPerformed(ActionEvent e)
		{
			addNewEvent();
		}
	});
	
	InputMap inMap = panel.getInputMap();
	inMap.put(KeyStroke.getKeyStroke("control N"), "addEvent");
	
	frame.getContentPane().add(panel);


	/**
	 * Dni tygodnia
	 */

	JLabel weekDay[] = new JLabel[7];
	
	weekDay[0] = new JLabel("Pon");
	weekDay[1] = new JLabel("Wto");
	weekDay[2] = new JLabel("Śro");
	weekDay[3] = new JLabel("Czw");
	weekDay[4] = new JLabel("Pią");
	weekDay[5] = new JLabel("Sob");
	weekDay[6] = new JLabel("Nie");
	
	for(int i=0; i < 7; i++)
	{
		weekDay[i].setBounds(12+i*60, 70, 60, 15);
		frame.getContentPane().add(weekDay[i]);
	}

		JPanel calendarPanel = new JPanel();
		calendarPanel.setBounds(10, 82, 403, 256);
		
		calendarPanel.setLayout(new GridLayout(6, 7, 0, 0));
		for (int i = 0; i < 42; i++) {
			buttons[i] = new JButton();
			
			buttons[i].addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JButton button = (JButton) e.getSource();
					Calendar cal = Calendar.getInstance();
					cal.setTime(currentlySelectedDate);
					
					int day;
					try
					{
						day = Integer.valueOf(button.getText());
					}
					catch(Exception exc)
					{
						return;
					}
					cal.set(Calendar.DAY_OF_MONTH, Integer.valueOf(button.getText()));
					
					List<Event> events = logic.getAllFromDay(cal.getTime());
					
					for(Event ev : events)
						showEvent(ev);
				}
			});
			
			calendarPanel.add(buttons[i]);
		}
		frame.getContentPane().add(calendarPanel);

		
		JButton eventAddButton = new JButton("Dodaj zdarzenie");
		eventAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			addNewEvent();
			}
		});
		eventAddButton.setBounds(10, 11, 159, 23);
		buttons2.add(eventAddButton);
		frame.getContentPane().add(eventAddButton);
		
		JButton removeOldEventsButton = new JButton("Usuń stare");
		removeOldEventsButton.setBounds(189, 11, 159, 23);
		removeOldEventsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JSpinner spinnerDate = new JSpinner();
				spinnerDate.setModel(new SpinnerDateModel(new Date(), new Date(959205600000L), new Date(7270552800000L), Calendar.DAY_OF_YEAR));
				JOptionPane.showMessageDialog(null, spinnerDate, "Podaj datę przed którą usunąć", 1);
		
				Date date = (Date)spinnerDate.getValue();
				removeEvents(date);
			}
		});
		buttons2.add(removeOldEventsButton);
		frame.getContentPane().add(removeOldEventsButton);
		
		JSpinner yearSpinner = new JSpinner();
		yearSpinner.setModel(new SpinnerDateModel(new Date(), new Date(959205600000L), new Date(7270552800000L), Calendar.MONTH));
		yearSpinner.setBounds(423, 148, 120, 20);
		yearSpinner.setEditor(new JSpinner.DateEditor(yearSpinner,"MMMM/yyyy"));
		
		yearSpinner.addChangeListener(new ChangeListener()
		{
			public void stateChanged(ChangeEvent e)
			{
				JSpinner sp = (JSpinner)e.getSource();
				dateChanged((Date)sp.getValue());
			}
		});
		
		frame.getContentPane().add(yearSpinner);
		
		JLabel lblWywietlaDatyZe = new JLabel("Miesiąc");
		lblWywietlaDatyZe.setBounds(423, 123, 103, 14);
		frame.getContentPane().add(lblWywietlaDatyZe);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("Plik");
		menuBar.add(fileMenu);
		
		JMenuItem saveToXMLItem = new JMenuItem("Zapisz do XML");
		saveToXMLItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String filePath = JOptionPane.showInputDialog("Wprowadz nazwe pliku:");
				saveXML(filePath);
			}
		});
		
		JMenuItem loadFromXMLItem = new JMenuItem("Odczytaj z XML");
		loadFromXMLItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filePath = JOptionPane.showInputDialog("Wprowadz nazwe pliku z danymi:");
				loadXML(filePath);
			}
		});
		
		
		JMenuItem saveToOdsItem = new JMenuItem("Zapisz do ods");
		saveToOdsItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filePath = JOptionPane.showInputDialog("Wprowadz nazwe pliku z danymi:");
				saveOds(filePath);
			}
		});
		
		fileMenu.add(saveToXMLItem);
		fileMenu.add(loadFromXMLItem);
		fileMenu.add(saveToOdsItem);
		
		
		JMenuItem settingsItem = new JMenuItem("Ustawienia");
		settingsItem.setAccelerator(KeyStroke.getKeyStroke('U', 
			Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		settingsItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openSettings();
			}
		});
		
		fileMenu.add(settingsItem);
		
		JMenuItem showAboutWindowMenuItem = new JMenuItem("O programie...");
		showAboutWindowMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAboutWindow();
			}
		}
			);

		fileMenu.add(showAboutWindowMenuItem);
		
		JMenuItem exitMenuItem = new JMenuItem("Wyjdź");
		exitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		fileMenu.add(exitMenuItem);
		
		dateChanged(new Date());
		frame.setVisible(true);
	}
//////////////////////////////////////////////////////////////////////////////////////////

/**
 * Saves the current application state into XML file.
 * @param filename Desired filename for XML output file. 	
 */

	@Override
	public void saveXML(String filename) {
		
		if(logic.saveToXML(filename) == 0)
			JOptionPane.showMessageDialog(frame, "Zapisano pomyślnie");
		else
			JOptionPane.showMessageDialog(frame, "Wystąpił‚ błąd zapisu");
	}
	/**
	 * Loads the organiser state from XML file with proper filename.
	 * 
	 * @param filename 	name of the file, under which program will attempt to save the current state.
	 */
		
	@Override
	public void loadXML(String filename) {
		
		if(logic.loadFromXML(filename) == 0)
			JOptionPane.showMessageDialog(frame, "Wczytano pomyślnie");
		else
			JOptionPane.showMessageDialog(frame, "Wystąpił‚ błąd odczytu");
			
		updateEvent();
	}
	
	/**
	 * Saves the current application state into ods file.
 	* @param filename Desired filename for ods output file. 	
 	*/

	@Override
	public void saveOds(String filename) {
		
		if(logic.saveToOds(filename) == 0)
			JOptionPane.showMessageDialog(frame, "Zapisano pomyślnie");
		else
			JOptionPane.showMessageDialog(frame, "Wystąpił‚ błąd zapisu");
	}
	
/**
 * Shows the "About Organizer" window.
 */
	@Override
	public void showAboutWindow() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(frame, "Organizer - prosty sposób na zarządzanie \n"+ 
				"				      Twoim czasem!                      \n" + 
				"				                                          \n" + 
				"		   Autorzy: \t \t \t \t \t \n" + 
				" \t \t \tHubert Sosnowski (Hurakano)  \n"
				+ " \t \t \tFilip Turoboś (Janfifian) ","O programie...",
			    JOptionPane.PLAIN_MESSAGE);
	}
	/**
	 * Opens the Creator of Events window in order to add new event
	 */
	@Override
	public void addNewEvent() {
		
		JSpinner spinnerDate = new JSpinner();
		spinnerDate.setModel(new SpinnerDateModel(new Date(), new Date(959205600000L), new Date(7270552800000L), Calendar.DAY_OF_YEAR));
		JOptionPane.showMessageDialog(null, spinnerDate, "Podaj datę zdarzenia", 1);
		
		Date date = (Date)spinnerDate.getValue();
		Event ev = new Event("", "", date);
		logic.addEvent(ev);
		updateEvent();
		showEvent(ev);
	}

	/**
	 * Removes all events older than selected date.
	 * 
	 * @param olderThan date, for which events preceeding it will be removed.
	 */
	@Override
	public void removeEvents(Date olderThan) {
		
		logic.removeOlder(olderThan);
		updateEvent();
	}

	/**
	 * Updates the calendar according to the event.
	 * This method works differently than the one in text
	 * interface due to the functionality of event display
	 * window.
	 */
	@Override
	public void updateEvent() {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentlySelectedDate);
		
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		int firstDay = cal.get(Calendar.DAY_OF_WEEK);
		firstDay--;
		if(firstDay == 0)firstDay = 7;
		
		for(int i = 1; i <= 42 ; i++)
		{
			if(i >= firstDay && i < maxDay+firstDay)
			{
				buttons[i-1].setText(String.valueOf(i-firstDay+1));
				
				cal.set(Calendar.DAY_OF_MONTH, i-firstDay+1);
				if(logic.getAllFromDay(cal.getTime()).size() > 0)
					buttons[i-1].setBackground(activeCalendarColor);
				else
					buttons[i-1].setBackground(calendarColor);
			}
			else
			{
				buttons[i-1].setText(new String());
				buttons[i-1].setBackground(calendarColor);
			}
		}
		
	}

/**
 * Changes the currently picked date in the application to the selected one.
 * Also, launches "updateEvent" method
 * @param date Date to be set as new one.
 */
	private void dateChanged(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		currentlySelectedDate = cal.getTime();
		
		updateEvent();
	}

	/**
	 * Launches the Event Display window.
	 * @param ev event for which the Event Display window
	 * will be launched.
	 */
	private void showEvent(Event ev)
	{
		IUserInterface ui = this;
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new EventDisplay(ev, logic, ui);
			}
		});
	}
	
	/**
	* Lunches settings window
	**/
	private void openSettings()
	{
		GraphicalInterface gui = this;
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SettingsWindow(gui, buttonColor, calendarColor, activeCalendarColor);
			}
		});
	}

	/**
	 * Reminds about upcoming event.
	 */
	@Override
	public void remind(Event ev)
	{
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(frame, "Pamiętaj o " + ev.getDescription());
	}
	
	/**
	* Changes color of buttons
	**/
	public void changeColor(Color a, Color b, Color c)
	{
		buttonColor = a;
		calendarColor = b;
		activeCalendarColor = c;
		
		for(JButton x : buttons2)
			x.setBackground(buttonColor);
			
		updateEvent();
	}
}
