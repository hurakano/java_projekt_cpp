package userInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;

class SettingsWindow extends JFrame
{
	private GraphicalInterface gui;
	
	private Color buttonColor = null;
	private Color calendarColor;
	private Color activeCalendarColor;
	
	private JButton buttonCol;
	private JButton buttonCal;
	private JButton buttonAct;
	
////////////////////////////////////////////
	
	public SettingsWindow(GraphicalInterface inter, Color a, Color b, Color c)
	{
		super("Ustawienia");
		gui = inter;
		buttonColor = a;
		calendarColor = b;
		activeCalendarColor = c;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(170, 250);
		setLayout(null);
		
		buttonCol = new JButton();
		buttonCol.setBounds(10, 10, 150, 40);
		buttonCol.setBorder(BorderFactory.createTitledBorder("kolor przyciskow"));
		buttonCol.setBackground(buttonColor);
		buttonCol.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				buttonColor = JColorChooser.showDialog(SettingsWindow.this, "Wybierz kolor przyciskow", buttonColor);
				buttonCol.setBackground(buttonColor);
			}
		
		});
		
		buttonCal = new JButton();
		buttonCal.setBounds(10, 60, 150, 40);
		buttonCal.setBorder(BorderFactory.createTitledBorder("kolor kafli"));
		buttonCal.setBackground(calendarColor);
		buttonCal.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				calendarColor = JColorChooser.showDialog(SettingsWindow.this, "Wybierz kolor kafli kalendarz", buttonColor);
				buttonCal.setBackground(calendarColor);
			}
		
		});
		
		buttonAct = new JButton();
		buttonAct.setBounds(10, 110, 150, 40);
		buttonAct.setBorder(BorderFactory.createTitledBorder("kolor aktywnych dni"));
		buttonAct.setBackground(activeCalendarColor);
		buttonAct.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				activeCalendarColor = JColorChooser.showDialog(SettingsWindow.this, "Wybierz kolor aktywnych dni", buttonColor);
				buttonAct.setBackground(activeCalendarColor);
			}
		
		});
		
		add(buttonCol);
		add(buttonCal);
		add(buttonAct);
		
		
		JButton buttClose = new JButton("OK");
		buttClose.setBounds(10, 160, 150, 40);
		buttClose.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				gui.changeColor(buttonColor, calendarColor, activeCalendarColor);
				dispatchEvent(new WindowEvent(SettingsWindow.this, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		add(buttClose);
		
		setVisible(true);
	}

}
