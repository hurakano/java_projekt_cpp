package calendar;

import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import java.io.*;
/**
 * Class used to load current state of application from an XML file.
 * @author Hurakano
 */
class FileLoaderXML implements LoaderInterface<EventContainer>
{
	String filename;
	/**
	 * Constuct a new FileLoaderXML.
	 * @param file determines the filename from which the XML state of the
	 * application will be loaded. This parameter will be later used in method
	 * implemented from LoaderInterface.
	 */
	public FileLoaderXML(String file)
	{
		filename = file;
	}
	/**
	 * Loads file from a determined in constructor file path.
	 * @return EventContainer which holds the data saved in read source file.
	 * @throws SaveLoadException in case the conversion to appropriate format
	 * was impossible or source was unreachable.
	 */
	@Override
	public EventContainer loadData() throws SaveLoadException
	{		
		EventContainer result;
		
		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(EventContainer.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
			result = (EventContainer) jaxbUnmarshaller.unmarshal( new File(filename));
		}
		catch(Exception x)
		{
			throw new SaveLoadException("Couldn't load from file: "+filename, x);
		}
		
		return result;
	}
}
