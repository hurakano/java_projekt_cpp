package calendar;

import userInterface.IUserInterface;

import java.util.Date;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;

/**
 *  This class provides main part of the Business Logic Layer of application.
 * @author Hurakano, JanFifian
 */
public class CalendarLogic
{
	private EventContainer events;
	
	private String databaseAddress = null;
	private String databaseUser = null;
	private String databasePassword = null;
	
	Timer remindTimer;
	Reminder reminder;
	

	/**
	 * Constructs new instance of the CalendarLogic class.
	 */
	public CalendarLogic()
	{
		events = new EventContainer();
		remindTimer = new Timer(true);//run as deamon
		reminder = null;
	}
	/**
	 *  Allows an object to hold reminders about incoming event.
	 * @param obj an object to set reminders for - a graphical or console interface.
	 */
	public void setRemindable(Remindable obj)
	{
		reminder = new Reminder(this, obj);
		remindTimer.schedule(reminder, 1000, 60000);//after second every minute
	}

	/**
	 * Constructs a new event with the desired parameters and adds it to the container.
	 * @param desc a short description of the event.
	 * @param place a place at which the event takes place.
	 * @param time date and time for the start of the event.
	 */
	public void addEvent(String desc, String place, Date time)
	{
		events.add(new Event(desc, place, time));
	}
	/**
	 * Adds an already constructed event to a container.
	 * 
	 * @param ev the event meant to be included in the container.
	 */
	public void addEvent(Event ev)
	{
		events.add(ev);
	}
	/**
	 * Returns all events stored in application.
	 * 
	 * @return list of all events stored in application. If the sort method
	 * has not been invoked even once, the order inside the returned container
	 * will match the order of the new events additions to the application.
	 */

	public List<Event> getAllEvents()
	{
		return events.getAllEvents();
	}
	/**
	 * Returns those events stored within the application, which date
	 * belongs to a given time interval.
	 * @param start starting point of the time interval.
	 * @param end ending point of the time interval.
	 * @return list of events for which the date does not exceed end
	 * and does not precede start.
	 */
	public List<Event> getEvents(Date start, Date end)
	{
		return events.getEventsByDate(start, end);
	}
	/**
	 * Sorts the EventContainer contents with respect to time.
	 */
	public void sort()
	{
		events.sortEvents();
	}
	
	/**
	 * Removes from EventContainer entries, which date precedes the chosen point.
	 * @param date point of time which determines the removal procedure end.
	 */
	public void removeOlder(Date date)
	{
		events.deleteOlder(date);
	}

	/**
	 * Sets the reminder for a given event to the hour defined by the arguments.
	 * 
	 * @param which index of event, for which the reminder is supposed to be set.
	 * @param hour hour of the remind.
	 * @param min minute of the remind.
	 */

	public void setRemind(int which, int hour, int min)
	{
		events.setRemind(which, hour, min);
	}

	/**
	 * Deletes the reminder for an event under a selected index of the container.
	 *
	 * @param which index of the event, which reminder is to be turned off.
	 */
	public void unsetRemind(int which)
	{
		events.unsetRemind(which);
	}
	
	/**
	 * Returns the list of all events stored for a given day.
	 * 
	 * @param date Day from which the events should be returned.
	 * @return List of events from the specified day.
	 */

	public List<Event> getAllFromDay(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		Date startDate = cal.getTime();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Date endDate = cal.getTime();
		
		return getEvents(startDate, endDate);
	}

	/**
	 * Removes selected event from the container
	 * @param ev Event to be removed
	 */
	public void removeEvent(Event ev)
	{
		events.remove(ev);
	}
	/**
	 * Sets credentials and address, which will be used to
	 * communicate with the database.
	 * @param adr The database path.
	 * @param user Username for database engine.
	 * @param pass Password for the provided username.
	 */

	public void setDatabaseAccess(String adr, String user, String pass)
	{
		databaseAddress = adr;
		databaseUser = user;
		databasePassword = pass;
	}
	/**
	 * Saves the state of the application under the parametrized filename
	 * to an XML file. This method uses the FileSaverXML object, defined 
	 * in a separate class.
	 * 
	 * @param filename determines the filename of the resulting file.
	 * @return returns 1 in case of failure, 0 otherwise.
	 */
	public int saveToXML(String filename)
	{
		SaverInterface<EventContainer> saver = new FileSaverXML(filename);
		
		try
		{
			saver.saveData(events);
		}
		catch(SaveLoadException x)
		{
			return 1;
		}
		
		return 0;
	}
	/**
	* Loads the state of the application from the file with name defined by
	* the function argument. The file is read as an XML file. This method 
	* uses the FileLoaderXML object, defined in a separate class.
	* 
	* @param filename determines the filename of the input file.
	* @return returns 1 in case of failure, 0 otherwise.
	*/
	public int loadFromXML(String filename)
	{
		LoaderInterface<EventContainer> loader = new FileLoaderXML(filename);

		try
		{
			events = loader.loadData();
		}
		catch(SaveLoadException x)
		{
			return 1;
		}
		
		return 0;
	}
	/**
	 * Saves the state of the application to MySQL database engine
	 * using the provided credentials.This method uses the 
	 * DatabaseSaver object, defined in a separate class.
	 * 
	 * @return returns 1 in case of failure,
	 * 2 in case the credentials were not provided,
	 * 0 otherwise.
	 */
	public int saveToDatabase()
	{
		if(databaseAddress == null && databaseUser == null && databasePassword == null)
			return 2;
			
		SaverInterface<EventContainer> saver = new DatabaseSaver(
			databaseAddress, databaseUser, databasePassword);
		
		try
		{
			saver.saveData(events);
		}
		catch(SaveLoadException x)
		{
			return 1;
		}
		
		return 0;
	}
	/**
	 * Loads the state of the application saved in MySQL database using
	 * the provided earlier credentials. This method uses the DatabaseLoader
	 * object, defined in a separate class.
	 * 
	 * @return returns 1 in case of failure,
	 * 2 in case the credentials were not provided,
	 * 0 otherwise.
	 */

	public int loadFromDatabase()
	{
	
		if(databaseAddress == null && databaseUser == null && databasePassword == null)
			return 2;
			
		LoaderInterface<EventContainer> loader = new DatabaseLoader(
			databaseAddress, databaseUser, databasePassword);
		
		try
		{
			events = loader.loadData();
		}
		catch(SaveLoadException x)
		{
			return 1;
		}
		
		return 0;
	}
/////////////////////////////////////////////////////////////////

	public int saveToOds(String filename)
	{
		SaverInterface<EventContainer> saver = new FileSaverOds(filename);
		
		try
		{
			saver.saveData(events);
		}
		catch(SaveLoadException x)
		{
			return 1;
		}
		
		return 0;
	}
}
