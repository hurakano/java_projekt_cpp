package calendar;
/**
 * 
 * An interface for classes, which are used to save the contents of
 * the program to a specific format.
 *
 * @param <thisType> type of output for the program to save its state.
 * Currently SQL database and XML outputs are supported.
 * @author Hurakano
 */
interface SaverInterface<thisType>
{
	/**
	 * Saves the data to the object of declared type.
	 * @param obj target object of declared type to contain saved data.
	 * @throws SaveLoadException in case the object is not accessible.
	 */
	void saveData(thisType obj) throws SaveLoadException;
}