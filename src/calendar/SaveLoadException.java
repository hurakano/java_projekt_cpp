package calendar;

import java.lang.Exception;

/**
 * A special type of exception for a saving and loading methods.
 * @author JanFifian
 */

class SaveLoadException extends Exception
{	
	/**
	 * Constructor for SaveLoadException using only message incorporated
	 * in standard Exception class construction.
	 * @param message a short description of the situation, which caused this exception to be raised.
	 */
	public SaveLoadException(String message)
	{
		super(message);
	}
	/**
	 * Constructor for SaveLoadException using both message and throwable
	 * incorporated in standard Exception class construction.
	 * @param message a short description of the situation, which caused this exception to be raised.
	 * @param cause a throwable object, suitable for the case.
	 */
	public SaveLoadException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
