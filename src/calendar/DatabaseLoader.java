package calendar;

import java.sql.*;
/**
 * Class used to load state of the application from MySQL database engine.
 * @author Hurakano
 */
class DatabaseLoader implements LoaderInterface<EventContainer>
{
	String databaseUrl;
	String username;
	String password;
	/**
	 * Constructs a new DatabaseLoader instance.
	 * @param db address of a database.
	 * @param user username of the users account in the given database.
	 * @param pass password, which will be used by database engine for authentication process.
	 */
	public DatabaseLoader(String db, String user, String pass)
	{
		databaseUrl = db;
		username = user;
		password = pass;
	}
	/**
	 * Loads application state from a database using the credentials provided during the construction of this object.
	 * @return EventContainer which holds the data saved in read source file.
	 * @throws SaveLoadException in case the conversion to appropriate format
	 * was impossible or source was unreachable.
	 */
	@Override
	public EventContainer loadData() throws SaveLoadException
	{
		Connection db = null;
		Statement stmt = null;
		
		EventContainer events = new EventContainer();
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			
			db = DriverManager.getConnection(databaseUrl, username, password);
			
			stmt = db.createStatement();
			String sql = "SELECT * FROM events";
			
			ResultSet result = stmt.executeQuery(sql);
			
			while(result.next())
			{
				String desc = result.getString("description");
				String place = result.getString("place");
				java.sql.Timestamp when = result.getTimestamp("kiedy");
				java.sql.Timestamp remind = result.getTimestamp("remind");
				
				Event ev = new Event(desc, place, when);
				if(remind != null) ev.setRemind(remind);
				
				events.add(ev);
			}
		}
		catch(Exception x)
		{
			throw new SaveLoadException("Couldn't load data", x);
		}
		finally
		{
			try
			{
				if(db != null)
					db.close();
				if(stmt != null)
					stmt.close();
			}
			catch(Exception x)
			{
			
			}
		}
		
		return events;
	}
}
