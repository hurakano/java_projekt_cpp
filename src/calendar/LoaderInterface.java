package calendar;
/**
 * 
 * An interface for classes, which are used to load the contents of
 * the program to a specific format.
 *
 * @param <thisType> type of input for the program to load its state.
 * Currently SQL database and XML inputs are supported.
 * @author Hurakano
 */
	
interface LoaderInterface<thisType>
{
	/**
	 * Loads file from a determined in implementing class file path.
	 * @return contained in an object saved data.
	 * @throws SaveLoadException in case the conversion to appropriate format was impossible or source was unreachable.
	 */
		 
	thisType loadData() throws SaveLoadException;
}
