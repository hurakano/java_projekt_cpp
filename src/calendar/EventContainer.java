package calendar;

import java.util.Date;
import java.util.List;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.Long;

import javax.xml.bind.annotation.*;
/**
 * Class which basically implements the data layer of the application.
 * It holds the events in a common container and allows to execute 
 * CRUD methods on them.
 * @author Hurakano, Janfifian
 */
@XmlRootElement(name = "eventContainer")
@XmlAccessorType (XmlAccessType.FIELD)
class EventContainer implements Collection<Event>
{
	@XmlElement(name = "event")
	List<Event> events = null;
	/**
	 * Constructs a new EventContainer instance.
	 */
	public EventContainer()
	{
		events = new ArrayList<Event>();
	}
	
	/**
	 * Adds a new event to the collection.
	 * 
	 * @param ev event to be added.
	 * 
	 * @return true if the operation was successful,
	 * false otherwise.
	 */
	@Override
	public boolean add(Event ev)
	{
		return events.add(ev);
	}
	
	/**
	 * Adds all events from a given collection of Event-type objects.
	 * @param evs collection of events to be included in the container.
	 * @return true if the operation was successful, false otherwise.
	 */
	@Override
	public boolean addAll(Collection<? extends Event> evs)
	{
		return events.addAll(evs);
	}
	
	/**
	 * Removes all currently held events from the container.
	 * Executing this method results in obtaining an empty
	 * container.
	 */
	@Override
	public void clear()
	{
		events.clear();
	}
	/**
	 * Checks whether the given object is included in the container.
	 * @param arg object to be looked for.
	 * @return true if the given object was found within the collection,
	 * false otherwise.
	 */
	@Override
	public boolean contains(Object arg)
	{
		return events.contains(arg);
	}
	/**
	 * Checks whether all objects from the given collection are included
	 * in the container.
	 * 
	 * @param arg a collection of objects to be looked for.
	 * 
	 * @return true if the each object from the given collection was found
	 * within the collection, false if there exists an object within the
	 * argument which could not be found within the EventContainer.
	 */
	@Override
	public boolean containsAll(Collection<?> arg)
	{
		return events.containsAll(arg);
	}
	/**
	 * Checks whether there are any events within the container.
	 * 
	 * @return true if no event was stored within the collection,
	 * false otherwise.
	 */
	@Override
	public boolean isEmpty()
	{
		return events.isEmpty();
	}
	/**
	 * Provides the iterator for the container.
	 * 
	 * @return iterator for the container.
	 */
	@Override
	public Iterator<Event> iterator()
	{
		return events.iterator();
	}
	/**
	 * Removes the first occurrence of the given object from a container.
	 * 
	 * @param object to be erased from the container.
	 * 
	 * @return true if operation was successful,
	 * false otherwise.
	 */
	@Override
	public boolean remove(Object arg)
	{
		return events.remove(arg);
	}
	/**
	 * Removes the contents of the provided collection from a container.
	 * 
	 * @param collection of objects to be erased from the container.
	 * 
	 * @return true if operation was successful i.e. container was modified,
	 * false otherwise.
	 */
	@Override
	public boolean removeAll(Collection<?> arg)
	{
		return events.removeAll(arg);
	}
	/**
	 * Provides the number of events stored within the container.
	 * 
	 * @return number of events stored within the container.
	 */
	@Override
	public int size()
	{
		return events.size();
	}
	/**
	 * Removes from the container all of its
	 * elements that are not from the provided collection.
	 * 
	 * @param collection of elements to intersect with the container.
	 * @return true if the container was modified during the process,
	 * false otherwise.
	 */
	@Override
	public boolean retainAll(Collection<?> arg)
	{
		return events.retainAll(arg);
	}
	/**
	 * Returns the container as an array.
	 * @return array of events stored within the container.
	 */
	@Override
	public Object[] toArray() 
	{
		return events.toArray();
	}
	/**
	 * Returns the container as an array.
	 * @param array to contain the events from the container.
	 * @return array of events stored within the container.
	 */
	@Override
	public <T> T[] toArray(T[] arg) 
	{
		return events.toArray(arg);
	}

	
	/**
	 * Returns those events stored within the container, which date
	 * belongs to a given time interval.
	 * @param start starting point of the time interval.
	 * @param end ending point of the time interval.
	 * @return list of events from the container for which the date
	 * does not exceed end and does not precede start.
	 */
	public List<Event> getEventsByDate(Date begin, Date end)
	{
		List<Event> ev = new ArrayList<Event>();
		
		for(Event item : events)
		{
			if(item.getDate().compareTo(begin) >= 0 && item.getDate().compareTo(end) <= 0)
				ev.add(item);
		}
		
		return ev;
	}
	/**
	 * Returns all events stored in container.
	 * 
	 * @return list of all events stored in container. If the sort method
	 * has not been invoked even once, the order inside the returned collection
	 * will match the order of the new events additions to the container.
	 */
	public List<Event> getAllEvents()
	{
		return this.getEventsByDate(new Date(0), new Date(Long.MAX_VALUE));
	}
	/**
	 * Sorts the event held in container with respect to date.
	 */
	public void sortEvents()
	{
		Collections.sort(events);
	}
	/**
	 * Deletes events from the collection which dates preceeds the given one.
	 * @param date date until which all the events are removed upon invoking
	 * this method.
	 */
	public void deleteOlder(Date date)
	{
		for(Iterator<Event> it = events.iterator(); it.hasNext(); )
		{
			Event ev = it.next();
			
			if(ev.getDate().compareTo(date) < 0)
				it.remove();
		}
	}
	/**
	 * Sets a remind date for an event under the given index within the container.
	 * @param which index of the event to set reminder for.
	 * @param hour full hours before the reminding.
	 * @param min minutes amount of minutes before the reminding. 
	 */
	public void setRemind(int which, int hour, int min)
	{
		events.get(which).setRemind(hour, min);
	}
	/**
	 * Deletes reminder for an event under a selected index of the container.
	 * @param which index of the event, which reminder is to be turned off.
	 */
	public void unsetRemind(int which)
	{
		events.get(which).deleteRemind();
	}

}
