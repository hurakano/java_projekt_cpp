package calendar;

import java.util.Date;
import javax.xml.bind.annotation.*;
/**
 * Class describing a single event to be contained in the organiser memory.
 * @author Hurakano
 */
@XmlAccessorType (XmlAccessType.FIELD)
public class Event implements Comparable<Event>
{
	String description;
	String place;
	Date date;
	Date remind;
	
	/**
	 * Constructs a new Event instance with all fields set to null.
	 */
	public Event()
	{
		this.description = null;
		this.place = null;
		this.date = null;
		this.remind = null;
	}
	/**
	 * Constructs a new event with a given description, place and date.
	 * @param desc a short description of the event.
	 * @param plc a place at which event takes place.
	 * @param time a date at which event takes place.
	 */
	public Event(String desc, String plc, Date time)
	{
		this.description = desc;
		this.place = plc;
		this.date = time;
		this.remind = null;
	}
	/**
	 * Gives the description of an event.
	 * @return the description of the event.
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * Sets the description of an event.
	 * @param desc the description of the event.
	 */
	public void setDescription(String desc)
	{
		this.description = desc;
	}
	/**
	 * Gives the place of an event.
	 * @return the place of the event.
	 */
	public String getPlace()
	{
		return place;
	}
	
	/**
	 * Sets the place of an event.
	 * @param plc the place of the event.
	 */
	public void setPlace(String plc)
	{
		this.place = plc;
	}
	/**
	 * Gives the date of an event.
	 * @return the date of the event.
	 */
	public Date getDate()
	{
		return date;
	}
	
	/**
	 * Sets the date of an event.
	 * @param time the date of the event.
	 */
	public void setDate(Date time)
	{
		this.date = time;
	}
	/**
	 * Gives the remind date of an event.
	 * @return the remind date of the event.
	 */
	public Date getRemind()
	{
		return remind;
	}
	
	/**
	 * Sets the remind date of an event.
	 * @param time the remind date of the event.
	 */
	
	public void setRemind(Date time)
	{
		this.remind = time;
	}
	
	/**
	 * Sets the remind date for an event, assuming that it should be reminded the 
	 * appropriate amount of time before it actually takes place. The behavior of
	 * this method remains fine even if hours or minutes parameters exceed the 
	 * desired values, i.e. 23 and 59 respectively.
	 * 
	 * @param hours full hours before the reminding.
	 * @param minutes minutes amount of minutes before the reminding. 
	 */
	
	
	public void setRemind(int hours, int minutes)
	{
		this.remind = new Date(this.date.getTime() - (hours*3600*1000 + minutes*60*1000));
	}
	
	/**
	 * Removes the remind date from an event by changing the remind field to null.
	 */
	public void deleteRemind()
	{
		this.remind = null;
	}
	/**
	 * Compares the current event to another event with respect to date.
	 * @param ev event to compare with.
	 * @return 0 if events take place exactly at the same time, 1 if the
	 * event which invokes this method takes place later than the compared
	 * event and -1 otherwise.
	 */
	@Override
	public int compareTo(Event ev)
	{
		return this.date.compareTo(ev.getDate());
	}
	/**
	 * Converts the current event to String object, so it can be written out.
	 * 
	 * @return returns the String description of the event.
	 */
	@Override
	public String toString()
	{
		String str = new String();
		
		str += getDescription() + " at " + getPlace();
		
		return str;
	}
}
