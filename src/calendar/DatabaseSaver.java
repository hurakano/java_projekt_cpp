package calendar;

import java.sql.*;
/**
* Class used to save state of the application to MySQL database engine.
* @author Hurakano
* 
*/
class DatabaseSaver implements SaverInterface<EventContainer>
{
	String databaseUrl;
	String username;
	String password;
	/**
	 * Constructs a new DatabaseSaver instance.
	 * @param db address of a database.
	 * @param user username of the users account in the given database.
	 * @param pass password, which will be used by database engine for authentication process.
	 */
	public DatabaseSaver(String db, String user, String pass)
	{
		databaseUrl = db;
		username = user;
		password = pass;
	}
	/**
	 * Saves the data to the XML object under filename determined 
	 * by filename variable.
	 * 
	 * @param obj target object containing data to be saved.
	 * @throws SaveLoadException in case the filename under which
	 * output was supposed to be saved is not accessible or simply
	 * incorrect.
	 */
	@Override
	public void saveData(EventContainer obj) throws SaveLoadException
	{
		Connection db = null;
		PreparedStatement stmt = null;
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			
			db = DriverManager.getConnection(databaseUrl, username, password);
			
			//stmt = db.prepareStatement("START TRANSACTION");
			//stmt.execute();
			stmt = db.prepareStatement("delete from events");
			stmt.execute();
			
			for(Event x : obj)
			{
				String sql = "INSERT INTO events(description, place, kiedy, remind) VALUES(?, ?, ?, ?)";
				stmt = db.prepareStatement(sql);
				stmt.setString(1, x.getDescription());
				stmt.setString(2, x.getPlace());
				stmt.setTimestamp(3, new java.sql.Timestamp(x.getDate().getTime()));
				
				if(x.getRemind() != null)
					stmt.setTimestamp(4, new java.sql.Timestamp(x.getRemind().getTime()));
				else
					stmt.setDate(4, null);
				
				stmt.execute();
			}
			
			//stmt = db.prepareStatement("COMMIT");
			//stmt.execute();
		}
		catch(Exception x)
		{
			throw new SaveLoadException("Couldn't save data", x);
		}
		finally
		{
			try
			{
				if(db != null)
				{	
					db.close();
				}
			}
			catch(Exception x)
			{
			
			}
		}
	}
}
