package calendar;

import java.util.Date;
import java.util.TimerTask;
/** 
 * 
 * Class describing tasks consisting of reminding the user about upcoming event.
 * 
 * @author Hurakano
 */
class Reminder extends TimerTask
{
	CalendarLogic events;
	Remindable remindTo;
	
	/**
	 * Constructor for the Reminder class objects.
	 * @param ev - event container, which will hold the events to remind about them.
	 * @param reTo - an user interface, which will remind the user about the event.
	 */
	Reminder(CalendarLogic evnts, Remindable reTo)
	{
		events = evnts;
		remindTo = reTo;
	}
	/**
	 * Using the appropriate user interface, this method reminds user about upcoming events.
	 */
	@Override
	public void run()
	{
		Date now = new Date();
		
		for(Event x : events.getAllEvents())
		{
			if(x.getRemind() != null)
				if(x.getRemind().compareTo(now) < 0)
				{
					remindTo.remind(x);
					x.deleteRemind();
				}
		}
	}
}
