package calendar;
/**
 *Interface for objects, which are capable of reminding about some incoming target time.
 * @author Hurakano
 */
public interface Remindable
{	
	/**
	 * Reminds the user about the incoming event.
	 * @param ev event to be recalled to user.
	 */
	void remind(Event ev);
}
