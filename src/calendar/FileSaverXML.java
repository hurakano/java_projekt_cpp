package calendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import java.io.*;
/**
 * Class used to save current state of application into an XML file.
 * @author Hurakano
 *
 */
class FileSaverXML implements SaverInterface<EventContainer>
{
	String filename;
////////////////////////////////
/**
 * Constuct a new FileSaverXML.
 * @param file determines the filename under which the XML output of the
 * application state will be saved. This parameter will be later used in method
 * implemented from SaverInterface.
 */
	public FileSaverXML(String file)
	{
		filename = file;
	}	
////////////////////////////////////////

	/**
	 * Saves the data to the XML object under filename determined 
	 * by filename variable.
	 * 
	 * @param obj target object containing data to be saved.
	 * @throws SaveLoadException in case the filename under which
	 * output was supposed to be saved is not accessible or simply
	 * incorrect.
	 */
	@Override
	public void saveData(EventContainer obj) throws SaveLoadException
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(EventContainer.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(obj, new FileWriter(filename));
		}
		catch(Exception x)
		{
			throw new SaveLoadException("Couldn't save to file: "+filename, x);
		}
	}
}
