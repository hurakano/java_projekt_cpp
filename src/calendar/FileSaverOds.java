package calendar;

import org.odftoolkit.simple.*;
import org.odftoolkit.simple.table.*;
import java.io.File;
import java.util.Calendar;

class FileSaverOds implements SaverInterface<EventContainer>
{
	String filename;
/////////////////////////////////////////////////////////////////////

	public FileSaverOds(String fn)
	{
		filename = fn;
	}
/////////////////////////////////////////////////////////////////////
	@Override
	public void saveData(EventContainer obj) throws SaveLoadException
	{
		try
		{
			SpreadsheetDocument doc = SpreadsheetDocument.newSpreadsheetDocument();
		
			Table tab = doc.getSheetByIndex(0);
			tab.setTableName("Events");
			
			Column col;
			
			col = tab.getColumnByIndex(2);
			col.setWidth(31);
			col = tab.getColumnByIndex(3);
			col.setWidth(31);
			
			Cell cell;
			
			//set headers
			cell = tab.getCellByPosition(0, 0);
			cell.setDisplayText("Opis");
			
			cell = tab.getCellByPosition(1, 0);
			cell.setDisplayText("Miejsce");
			
			cell = tab.getCellByPosition(2, 0);
			cell.setDisplayText("Kiedy");
			
			cell = tab.getCellByPosition(3, 0);
			cell.setDisplayText("Przypomnienie");
			
			//set events
			Calendar cal = Calendar.getInstance();
			int i = 1;
			
			for(Event ev : obj)
			{
				cell = tab.getCellByPosition(0, i);
				cell.setDisplayText(ev.getDescription());
				
				cell = tab.getCellByPosition(1, i);
				cell.setDisplayText(ev.getPlace());
				
				cell = tab.getCellByPosition(2, i);
				cal.setTime(ev.getDate());
				cell.setDateTimeValue(cal);
			
				cell = tab.getCellByPosition(3, i);
				if(ev.getRemind() != null)
				{
					cal.setTime(ev.getRemind());
					cell.setDateTimeValue(cal);
				}
				else
					cell.setDisplayText("No remind");
					
				i++;
			}
		
			doc.save(new File(filename));
		}
		catch(Exception e)
		{
			throw new SaveLoadException("Couldn't save file", e);
		}
	}
}
